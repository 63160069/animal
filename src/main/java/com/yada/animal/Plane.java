/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.animal;

/**
 *
 * @author ASUS
 */
public class Plane extends Vehicle implements Flyable,Runable {

    public Plane(String engine) {
        super(engine);
    }

    @Override
    public void startEngine() {
        System.out.println("Plane: " + getEngine()+  " start engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Plane: " + getEngine()+  " stop engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Plane: " + getEngine()+  " raise speed");
    }

    @Override
    public void applyBreak() {
        System.out.println("Plane: " + getEngine()+  " apply break");
    }

    @Override
    public void fly() {
        System.out.println("Plane: " + getEngine()+  " fly");
    }

    @Override
    public void run() {
        System.out.println("Plane: " + getEngine()+  " run");
    }
    
}
