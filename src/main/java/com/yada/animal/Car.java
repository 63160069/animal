/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.yada.animal;

/**
 *
 * @author ASUS
 */
public class Car extends Vehicle implements Runable {

    public Car(String string) {
        super(string);
    }

    @Override
    public void startEngine() {
        System.out.println("Car: " + getEngine()+  " start engine");
    }

    @Override
    public void stopEngine() {
        System.out.println("Car: " + getEngine()+  " start engine");
    }

    @Override
    public void raiseSpeed() {
        System.out.println("Car: " + getEngine()+  " start engine");
    }

    @Override
    public void applyBreak() {
        System.out.println("Car: " + getEngine()+  " apply break");
    }

    @Override
    public void run() {
        System.out.println("Car: " + getEngine()+  " run");
    }
    
}
